#!/usr/bin/env python3

"""Exemple d'héritage multiple"""

class Vehicule(object):
    def __init__(self, immat):
        self.immat = immat

    def avance(self, distance):
        print("avance dans Vehicule")

class Bateau(Vehicule):
    def __init__(self, immat):
        Vehicule.__init__(self, immat)

    def avance(self, distance):
        print("avance dans Bateau")

class Voiture(Vehicule):
    def __init__(self, immat):
        Vehicule.__init__(self, immat)

    def avance(self, distance):
        print("avance dans Voiture")

class VoitureAmphibie(Bateau, Vehicule):
    pass

if __name__ == '__main__':
    va = VoitureAmphibie("VA123")
    va.avance(12)
    print(va.immat)
